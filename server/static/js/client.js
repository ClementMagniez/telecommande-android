var VID_ID=''; // ID de la vidéo actuellement lancée
var PREVIOUS_ID=''; // ID de la vidéo précédant l'actuelle (identique si une seule vidéo)
var HAS_LOADED=false; // Vérifie que l'API a été chargée
var HAS_STARTED=false; // Vérifie que l'iframe 'player' a été créée 

if(!!window.EventSource) {
  var source=new EventSource('/')
  source.onmessage=function(e) {
		res=e.data.split(',');
		if(res[0]=="id") {
			VID_ID=new String(res[1]);
		
			if(!HAS_LOADED && !HAS_STARTED) { // charge l'API - exécuté une seule fois
				load();
			}
			else if (HAS_STARTED && res[1]!=PREVIOUS_ID) { // remplace la vidéo actuelle sans recharger l'API
				updatePlayer(); 
			}
		}
		else if(res[0]=="pause") { player.pauseVideo(); }
		else if(res[0]=="play")  { player.playVideo(); }
		else if(res[0]=="on")    { player.unMute(); }
		else if(res[0]=="off")   { player.mute(); }
		else if(res[0]=="up")    { player.setVolume((player.getVolume()+5)%100); }
		else if(res[0]=="down")  {	player.setVolume(Math.abs(player.getVolume()-5)); }
		else if(res[0]=="seekto")   { player.seekTo(Number(res[1]), true); }

	}
}


function load() {
	var tag = document.createElement('script');
	tag.src = "https://www.youtube.com/iframe_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
	HAS_LOADED=true;
}

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player;
function onYouTubeIframeAPIReady() {
	player = new YT.Player('player', {
    width: '1920',
    height: '907',
    videoId: VID_ID,
    events: {
      'onReady': onPlayerReady,
      'onStateChange': onPlayerStateChange,
    }
  });
}

// Met à jour la vidéo dans le player pré-existant
function updatePlayer() {
	player.loadVideoById(VID_ID, 0);
	PREVIOUS_ID=VID_ID;
}

function sendTimers(){
	dur=Math.trunc(player.getDuration());
	current_time=Math.trunc(player.getCurrentTime());
	$.post( "/", { time:current_time, duration: dur});
}


// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
	if(!HAS_STARTED) 
		$.get( "/", { });
	HAS_STARTED=true;
	PREVIOUS_ID=VID_ID;
  event.target.playVideo();
}

function onPlayerStateChange(event) {
	if (event.data == YT.PlayerState.PLAYING) {
			sendTimers();
	}
}

