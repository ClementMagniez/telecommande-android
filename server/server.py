import socket
from flask import Flask, request, Response, render_template
from threading import Thread
import time
import webbrowser
import json
import pynput.keyboard as pynputk

HOST="127.0.0.1"
FLASK_PORT=8005
MOBILE_PORT=8000
MOBILE_DATA=""
FLASK_UI_DATA=None

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def index():
	global FLASK_UI_DATA
	if request.headers.get('accept')=='text/event-stream':
		def events():
			global MOBILE_DATA
			while True:
				if MOBILE_DATA!="":
					yield f"data: {MOBILE_DATA}\n\n"
					MOBILE_DATA=""
				time.sleep(.1)
		return Response(events(), content_type='text/event-stream')
	if request.method == 'POST':
		FLASK_UI_DATA = request.form['duration']+" "+request.form['time']+" "
	if request.method == 'GET':
		keyboard=pynputk.Controller()
#		keyboard.press(pynputk.Key.f11)
#		keyboard.release(pynputk.Key.f11)
	return render_template('index.html')

def connexion_mobile():
	global MOBILE_DATA, FLASK_UI_DATA
	with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
		s.bind((HOST, MOBILE_PORT))
		s.listen()
		while(True):
			conn, addr= s.accept()
			with conn:
				print('connecté à ', addr)
				Thread(target=send_to_mobile, args=(conn,HOST, MOBILE_PORT)).start()
				while(True):
					data=conn.recv(1024)
					MOBILE_DATA=data.decode();
					if not data:
						break

def send_to_mobile(conn, host, port):
	global FLASK_UI_DATA
	while(True): # serait plus efficient de notifier la méthode dans index() - attente passive
		if(FLASK_UI_DATA!=None):
			try:				
				conn.sendall(bytes(FLASK_UI_DATA, 'utf8'))
				FLASK_UI_DATA=None
			except OSError:
				exit() # socket fermée : un autre thread sera créé à la prochaine connexion

def flask_thread():
	app.run(host=HOST, port=FLASK_PORT, threaded=True)

if __name__ == "__main__":
	thread_conn_phone=Thread(target=connexion_mobile)
	thread_conn_phone.start();
	thread_flask=Thread(target=flask_thread)
	thread_flask.start()
	webbrowser.open("http://"+HOST + ":" + str(FLASK_PORT))
	thread_flask.join()
	thread_conn_phone.join()
