package com.example.telecommande;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class NetworkService extends Service {
    private final IBinder mBinder = new LocalBinder();
    private PrintWriter pout;
    private Socket socket;
    private final String HOST="10.0.2.2";
    private final int PORT=8000;

    private SocketSenderThread threadSender;
    private volatile boolean isConnected=false;


    @Override
    public void onCreate() {
        super.onCreate();
        Log.i("service", "creation service");
        this.openSocket();
    }

    @Override
    public void onDestroy() {
        this.closeSocket();
        super.onDestroy();
    }

    public IBinder onBind(Intent intent) {
        Log.i("service", "bind service");
        if(socket==null)
            this.openSocket();
        return mBinder;
    }
    /** Crée et lance un SocketStarterThread */
    public void openSocket() {
        Thread connOpener=new SocketStarterThread();
        connOpener.start();
    }

    /**
     * Met isConnected à false (interrompt un SocketStarterThread) et ferme socket
     */
    public void closeSocket() {
        Log.i("service", "closing socket");
        this.isConnected=false;
        try {
            if(socket!=null) {
                socket.close();
                socket=null;
            }
        }
        catch(IOException e) {
            Log.i("service", "socket closure fail");
        }
    }


    class LocalBinder extends Binder {
        NetworkService getService() {
            return NetworkService.this;
        }
    }

    /**
     * Thread créant une socket TCP se connectant à un serveur donné, sur un port donné
     */
    private class SocketStarterThread extends Thread {
        /**
         * Initialise NetworkService#socket et NetworkService#pout
         */
        public void run() {
            int port=PORT;
            String host=HOST;
 /*           if(sharedPref!=null) {
                port = sharedPref.getInt("host", PORT);
                host = sharedPref.getString("host", HOST);
            }
*/
            try {
                socket=new Socket(host, port);
                Log.i("socketstarter", "creation socket");
            }
            catch(Exception e) {
                Log.i("socketstarter", e.toString());
                Log.i("socketstarter", "cause probable : serveur non-lancé");
            }
            try {
                pout=new PrintWriter(socket.getOutputStream(), true);
                Log.i("socketstarter", "creation writer");
            }
            catch(Exception e) {
                Log.i("socketstarter", e.toString());
            }
            isConnected=true;
        }
    }

    /**
     * Initie et lance un SocketSenderThread avec msg ainsi qu'un SocketReceiverThread
     * @param msg String : premier message à envoyer à la création du thread
     */
    public void startThread(String msg) {
        if(this.socket==null)
            return;

        this.threadSender =new SocketSenderThread(msg);
        this.threadSender.start();
        SocketReceiverThread threadReceiver = new SocketReceiverThread();
        threadReceiver.start();
    }

    /**
     * Met à jour le message à envoyer par NetworkService#threadSender
     * @param msg un message à envoyer au serveur
     */
    public void setMsg(String msg) {
        if(this.threadSender!=null)
            this.threadSender.setMsg(msg);
    }

    /**
     * Ecoute le serveur connecté à NetworkService#socket ; à la réception d'un message,
     * broadcast sur TelecommandeActivity#RECEIVE_TIMINGS
     */
    private class SocketReceiverThread extends Thread {

        SocketReceiverThread() {
            super();
        }

        /**
         * Broadcast une intent sur TelecommandeActivity#RECEIVE_TIMINGS
         * Contenu de l'intent :
         * String : "duration", durée totale de la vidéo
         * String : "current_time", placement actuel dans la vidéo
         */
        public void run() {
            byte[] dataReceived=new byte[1024];
            while(isConnected) {
                try {
                    socket.getInputStream().read(dataReceived);
                    String msg = new String(dataReceived, StandardCharsets.UTF_8);
                    String[] split_msg=msg.split(" ");
                    Log.i("receive", msg);
                    Intent intent=new Intent(TelecommandeActivity.RECEIVE_TIMINGS);
                    intent.putExtra("duration", split_msg[0]);
                    intent.putExtra("current_time", split_msg[1]);
                    sendBroadcast(intent);
                }
                catch(IOException e) {
                    Log.i("receive", e.toString());
                }
                catch(ArrayIndexOutOfBoundsException | NullPointerException e) {
                    Log.i("receive", e.toString());
                    isConnected=false; // réception d'un message imprévu -> serveur down

                    Looper.prepare();
                    final Context context = getApplicationContext();
                    final CharSequence text = getResources().getString(R.string.noServerToast);
                    final Handler mHandler = new Handler() {
                        public void handleMessage(Message msg) {
                            Toast toast=Toast.makeText(context, text, Toast.LENGTH_LONG);
                            toast.show();
                        }
                    };

                    Looper.loop();

                }
            }
        }
    }

    /**
     * Transmet au serveur connecté à NetworkService#broadcast
     */
    private class SocketSenderThread extends Thread {
        private volatile String msg;
        private volatile boolean msgHasChanged;
        private final Object msgMutex=new Object();
        private SocketSenderThread(String msg) {
            super();
            Log.i("send", "creation sender");
            this.setMsg(msg);
        }

        /**
         * Setter de this.msg, synchronisé ; met msgHasChanged à true
         */
        void setMsg(String msg) {
            synchronized (msgMutex) {
                Log.i("send", "update msg : "+this.msg+" to "+msg);
                this.msg = msg;
                this.msgHasChanged=true;
            }
        }

        /**
         * Transmet this.msg au serveur si this.msgHasChanged est true
         * S'arrête si NetworkService#isConnected est false
         */
        @Override
        public void run() {
            Log.i("send", "loop envoi lancé");
            while(isConnected) {
                synchronized(msgMutex) {
                    if (this.msgHasChanged) {
                        Log.i("send", "envoi msg : " + this.msg);
                        pout.print(this.msg);
                        pout.flush();
                        this.msgHasChanged=false;
                    }
                }
            }
        }
    }
}
