package com.example.telecommande;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class TelecommandeActivity extends AppCompatActivity {

    // ID d'envoi/réception de la durée de la vidéo en broadcast
    public static final String RECEIVE_TIMINGS="com.example.timings";

    // Vérifie que networkService est bound
    private boolean mBound = false;
    // Vérifie que la seekbar doit être mise à jour chaque seconde
    private boolean updateSeekbarRunning=true;
    private boolean updateSeekbarHasStarted=false;
    // Gère la communication avec un NetworkService
    NetworkService networkService;

    // Gère la réception de la durée de la vidéo
    BroadcastReceiver updateUIReceiver;

    // TODO Voir dans quelle mesure décaler tout dans this.onCreate
    /** Bind networkService ; initie les listeners des boutons, updateUIReceiver, la seekbar
     * onServiceConnected agit une fois que le player YT a notifié qu'il est disponible,
     * on préfère donc tout initier ici plutôt qu'au onCreate
      */
    private ServiceConnection connection = new ServiceConnection() {
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            NetworkService.LocalBinder binder = (NetworkService.LocalBinder) service;
            networkService = binder.getService();
            mBound = true;

            final BooleanButton playPause=findViewById(R.id.playpause_button);
            playPause.setNetworkThread(networkService);

            final BooleanButton soundOnOff=findViewById(R.id.onoff_sound_button);
            soundOnOff.setNetworkThread(networkService);

            final ButtonAdjustVolume volumeUp=findViewById(R.id.buttonUp);
            volumeUp.setNetworkThread(networkService);

            final ButtonAdjustVolume volumeDown=findViewById(R.id.buttonDown);
            volumeDown.setNetworkThread(networkService);

            final IntentFilter filter = new IntentFilter();
            filter.addAction(RECEIVE_TIMINGS);


            final TextView startSeekBar=findViewById(R.id.startSeekBar);
            final TextView endSeekBar=findViewById(R.id.endSeekBar);

            final SeekBar seekBar=findViewById(R.id.seekBar);
            seekBar.setMin(0);
            final Thread threadSeekbar=new ThreadUpdateSeekbar(seekBar, playPause);

            seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                    if(fromUser) {
                        networkService.setMsg("seekto, "+ progress);
                    }

                    updateSeekbar(0, progress);

                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {

                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {

                }
            });

            updateUIReceiver = new BroadcastReceiver() {
                @RequiresApi(api = Build.VERSION_CODES.O)
                @Override
                public void onReceive(Context context, Intent intent) {
                    if(intent.getAction().equals(RECEIVE_TIMINGS)) {
                        Bundle data=intent.getExtras();
                        int duration=Integer.parseInt(data.getString("duration"));
                        int current_time=Integer.parseInt(data.getString("current_time"));

                        seekBar.setMax(duration);
                        updateSeekbar(duration, current_time);
                        if(!updateSeekbarHasStarted) {
                            threadSeekbar.start();
                            updateSeekbarHasStarted=true;
                        }

                    }

                }
            };
            registerReceiver(updateUIReceiver,filter);
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    protected void onStop() {
        super.onStop();
        this.doUnbindService();
        this.stopSeekBarThread();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.doUnbindService();
        this.unregisterReceiver(updateUIReceiver);
        this.stopSeekBarThread();
    }

    /**
     * Met à jour les TextView décrivant la seekbar
     * @param duration la durée totale de la vidéo (secondes) - 0 si pas à modifier
     * @param current_time le placement actuel dans la vidéo (secondes) - 0 si pas à modifier
     */
    private void updateSeekbar(int duration, int current_time) {
        final TextView startSeekBar=findViewById(R.id.startSeekBar);
        final TextView endSeekBar=findViewById(R.id.endSeekBar);

        String dur_str=String.format("%02d:%02d", duration / 60, duration % 60);
        String curr_str=String.format("%02d:%02d", current_time / 60, current_time % 60);

        Log.i("format", dur_str+" "+curr_str);

        if(duration!=0)
            endSeekBar.setText(dur_str);
        if(current_time!=0)
        startSeekBar.setText(curr_str);


    }

    /** Interrompt le thread ThreadUpdateSeekbar actif en mettant updateSeekbarRunning à false
     *
     */
    private void stopSeekBarThread() {
        this.updateSeekbarRunning=false;
    }

    /**
     * Unbind networkService
     */
    private void doUnbindService() {
        if(mBound) {
            unbindService(connection);
            mBound = false;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telecommande);

        Intent serviceIntent=new Intent(TelecommandeActivity.this,NetworkService.class);
        bindService(serviceIntent, connection, Context.BIND_AUTO_CREATE);


        ImageButton button_img_search = findViewById(R.id.imgSearch);
        button_img_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rechercherVideos();
            }
        });

        ImageButton button_img_home = findViewById(R.id.imgHome);
        button_img_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ouvrirMenuHome();
            }
        });

    }

    /**
     * Thread incrémentant une seekbar de 1 chaque seconde tant que la vidéo YT est jouée sur le serveur
     */
    private class ThreadUpdateSeekbar extends Thread {
        private SeekBar seekbar;
        private BooleanButton buttonPause;

        /**
         * Constructeur
         * @param sb Seekbar à update
         * @param bp BooleanButton contenant les messages "play" et "pause"
         */
        ThreadUpdateSeekbar(SeekBar sb, BooleanButton bp) {
            this.seekbar=sb;
            this.buttonPause=bp;
        }

        /**
         * Incrémente la seekbar de 1 par seconde
         */
        @Override
        public void run() {
            while(updateSeekbarRunning) {
                try {
                    sleep(1000);
                    if("play".equals(buttonPause.getMessage())) {
                        this.seekbar.incrementProgressBy(1);
                    }
                }
                catch(InterruptedException e) {
                    Log.i("seekbar", "thread interrompu");
                    Log.i("seekbar", e.toString());
                    break;
                }
            }
        }
    }
    public void rechercherVideos(){
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    public void ouvrirMenuHome(){
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
