package com.example.telecommande;

import android.content.Context;
import android.util.AttributeSet;

import androidx.core.content.ContextCompat;

public class ButtonVolumeOnOff extends BooleanButton {

    public ButtonVolumeOnOff(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.configure("on", "off",
                R.drawable.ic_volume,
                R.drawable.ic_volume_off);
    }
}

