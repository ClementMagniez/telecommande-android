package com.example.telecommande;

import android.content.Context;
import android.util.AttributeSet;

public class ButtonVolumeUp extends ButtonAdjustVolume {
    public ButtonVolumeUp(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.configure("up");
    }
}
