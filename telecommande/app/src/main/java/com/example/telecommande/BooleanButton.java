package com.example.telecommande;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageButton;

public class BooleanButton extends AppCompatImageButton {

    private String[] message=new String[2];
    private int[] drawable=new int[2];
    private int iter=0;

    public BooleanButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void configure(final String msg1, final String msg2, final int img1, final int img2) {
        this.message[0]=new String(msg1);
        this.message[1]=new String(msg2);
        this.drawable[0]=img1;
        this.drawable[1]=img2;
        Log.i("send", this.message.toString());
    }

    public void setNetworkThread(final NetworkService networkService) {
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("send", "msg à send au clic : "+message[iter]);
                iter=1-iter;
                networkService.setMsg(message[iter]);
                BooleanButton.this.setImageResource(drawable[iter]);
            }
        });
    }

    public int getIter() {
        return this.iter;
    }
    public String getMessage()  {
        return this.message[iter];
    }

}
