package com.example.telecommande;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class MainActivity extends Activity {
    public static SharedPreferences sharedPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ImageButton button_img_telecommande = findViewById(R.id.imgRemote);
        button_img_telecommande.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMenuTelecommande();
            }
        });

        Button button_mode_emploi = findViewById(R.id.buttonModeEmploi);
        button_mode_emploi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openModeEmploi();
            }
        });

        Button button_recherche = findViewById(R.id.buttonRecherche);
        button_recherche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rechercherVideos();
            }
        });

/**        sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("port", 8000);
        editor.putString("server", "10.0.2.2");
        editor.apply();
*/
    }

    public void rechercherVideos(){
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    public void openModeEmploi(){
        Intent intent = new Intent(this, ModeEmploiActivity.class);
        startActivity(intent);
    }

    public void openMenuTelecommande(){
        Intent intent = new Intent(this, TelecommandeActivity.class);
        startActivity(intent);
    }
}