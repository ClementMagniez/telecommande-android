package com.example.telecommande;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

/** Décrit un menu de recherche permettant d'input un mot-clé et un nombre attendu de résultats
 *  Affiche une liste des résultats fournis par la recherche effectuée via l'API Youtube
 */
public class SearchActivity extends AppCompatActivity {

    private HandlerExtension handler; // gère la communication avec un NetworkThread
    NetworkService networkService; // gère la communication avec un NetworkService
    boolean mBound = false; // vérifie que networkService est bound

    // (un)bind NetworkService
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {

            NetworkService.LocalBinder binder = (NetworkService.LocalBinder) service;
            networkService = binder.getService();
            mBound = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_scrolling);
        handler=new HandlerExtension(this);
        final TextView searchResult = findViewById(R.id.search_field);
        final TextView nbResults=findViewById(R.id.nb_results_search);
        Button searchButton =findViewById(R.id.search_button);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int nbRes=10; // valeur par défaut si champ vide
                try {
                    nbRes=Integer.parseInt(nbResults.getText().toString());
                }
                catch(NumberFormatException e) {
  /*TODO créer une méthode showToast générique et pouvant être appelée dans un thread*/
                    Context context = getApplicationContext();
                    CharSequence text = getResources().getString(R.string.noNbResultToast);
                    Toast toast = Toast.makeText(context, text, Toast.LENGTH_SHORT);
                    toast.show();
                }
                catch(Exception e) {
                    Log.e("exception", "nbResults erroné");
                }
                if(nbRes== 0) nbRes=10;
                NetworkThread searchThread = new NetworkThread(searchResult.getText().toString(), nbRes);
                searchThread.start();
            }
        });
        Intent serviceIntent=new Intent(SearchActivity.this,NetworkService.class);
        startService(serviceIntent);
        bindService(serviceIntent, connection, Context.BIND_AUTO_CREATE);
    }

    /** Ajoute au layout de l'activité un linear layout vertical servant à lister les résultats
     *  de recherche contenus dans data
     * @param data Bundle contenant :
     *             un array de Bitmap ("thumbnails") ;
     *             un array de String ("title") ;
     *             un array de String ("description") ;
     *             un array de String ("ID")
     */

    private void showResults(Bundle data) {

        // TODO tester sans Internet et gérer les erreurs (null pointer dans le bundle)

        int nbResults=data.getInt("nbResults");

        LinearLayout[] vidDisplay=new LinearLayout[nbResults];
        final Bitmap[] thumbnailsBit=(Bitmap[])data.getParcelableArray("thumbnails");
        final String[] vidTitle=data.getStringArray("title");
        final String[] vidDescription=data.getStringArray("description");
        final String[] vidId=data.getStringArray("ID");

/*      Workaround : si on ne refresh pas pageDisplay à chaque recherche, les résultats précédents
        restent affichés, on doit les supprimer - ce qui supprime aussi search_box
        qu'on remet donc en place */
        LinearLayout searchBox=findViewById(R.id.search_box);
        LinearLayout pageDisplay=findViewById(R.id.pagelayout);
        pageDisplay.removeAllViews();
        pageDisplay.addView(searchBox);
        for( int i=0;i<nbResults;i++) {
            try {

            ImageView thumbnail=new ImageView(this);
            TextView titleDesc=new TextView(this);
            titleDesc.setText(String.format("\n%s\n%s", vidTitle[i], vidDescription[i].substring(0, Math.min(vidDescription[i].length(), 50))));
            titleDesc.setTextColor(Color.parseColor("#E73E3A"));
            thumbnail.setImageBitmap(thumbnailsBit[i]);

            vidDisplay[i] = new LinearLayout(this);
            vidDisplay[i].setOrientation(LinearLayout.HORIZONTAL);
            vidDisplay[i].addView(thumbnail);
            vidDisplay[i].addView(titleDesc);
            vidDisplay[i].setClickable(true);

            final int finalI = i;
            vidDisplay[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    networkService.startThread("id,"+vidId[finalI]);
                    Intent swapToTele=new Intent(getBaseContext(), TelecommandeActivity.class);
                    startActivity(swapToTele);
                        }
                    }
                );

            thumbnail.getLayoutParams().height=300;
            thumbnail.getLayoutParams().width=350;

            pageDisplay.addView(vidDisplay[i]);
            }
            catch(NullPointerException e) {
                Log.i("search", e.toString());
                Log.i("search", String.valueOf(nbResults));
                Log.i("search", String.valueOf(i));
                Log.i("search", "cause probable : API YT");
            }
        }
    }

    /** Permet la communication entre NetworkThread et SearchActivity
     *
     */
    private static class HandlerExtension extends Handler {
        private final WeakReference<SearchActivity> currentActivity;

        private HandlerExtension(SearchActivity activity){
            currentActivity = new WeakReference<SearchActivity>(activity);
        }

        /** Appelle SearchActivity#showResults sur le contenu de message
         *
         * @param message Message envoyé par un NetworkThread contenant le bundle décrit dans showResults
         */
        @Override
        public void handleMessage(Message message){
            SearchActivity activity = currentActivity.get();
            if (activity!= null){
                activity.showResults(message.getData());
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.doUnbindService();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.doUnbindService();
    }

    /** Stoppe la connexion avec le serveur distant et unbind networkService
     *
     */
    private void doUnbindService() {
        if(mBound) {
            unbindService(connection);
            mBound = false;
        }
    }


    /** Effectue une recherche sur un mot-clé via l'API Youtube
     *
     */
    private class NetworkThread extends Thread {
        private String keyword;
        private int nbResults;

        /**
         * Constructeur
         * @param keyword le(s) mot(s)-clé à rechercher
         * @param nbResults le nombre de résultats attendus
         */
        private NetworkThread(String keyword, int nbResults) {
            this.keyword=keyword;
            this.nbResults =nbResults;
        }

        /**
         * Appelle PrivateHandler#sendMessage sur le résultat de la requête API exécutée
         */
        @Override
        public void run() {

            // Requête permettant spécifiquement une recherche par keyword, via la clé API du projet
            // Choisir le nombre de résultats : ?maxResults=[0,50]
            String sURL = "https://www.googleapis.com/youtube/v3/search?part=snippet&q="+keyword+"&maxResults="+ nbResults +
                          "&type=video&key=AIzaSyD4vqCxdItecvy6dk0UKvLLfHv4UT5PdqA";

            URLConnection request = null;
            URL url=null;
            JsonElement root = null;

            try {
                url = new URL(sURL);

            }
            catch (MalformedURLException e) {
                Log.e("search", e.toString());
            }

            try {
                request=url.openConnection();
                request.connect();

            }
            catch(Exception e) {
                Log.e("search", e.toString());
/* TODO A exécuter dans le thread UI
                Context context = getApplicationContext();
                CharSequence text = getResources().getString(R.string.noInternetToast);
                Toast toast = Toast.makeText(context, text, Toast.LENGTH_LONG);
                toast.show();
*/
                return;
            }

            try {
                root = JsonParser.parseReader(new InputStreamReader((InputStream) request.getContent()));
            }
            catch(IOException e) {

                Log.e("IO", e.toString());
            }
            JsonArray listeVids;

            if(root==null)
                return;
            listeVids= root.getAsJsonObject().getAsJsonArray("items");
            nbResults=Math.min(listeVids.size(), nbResults);
            String[] vidId=new String[this.nbResults];
            String[] vidThumbnails=new String[this.nbResults];
            String[] vidTitle=new String[this.nbResults];
            String[] vidDescription=new String[this.nbResults];

            Bitmap[] bmp=new Bitmap[this.nbResults];

            for (int i = 0; i< this.nbResults; i++) {
                vidId[i]=listeVids.get(i)
                        .getAsJsonObject().get("id")
                        .getAsJsonObject().get("videoId")
                        .toString();
                vidId[i]=vidId[i].substring(1, vidId[i].length()-1);

                vidTitle[i]=listeVids.get(i)
                        .getAsJsonObject().get("snippet")
                        .getAsJsonObject().get("title")
                        .toString();
                vidTitle[i]=vidTitle[i].substring(1, vidTitle[i].length()-1);

                vidDescription[i]=listeVids.get(i)
                        .getAsJsonObject().get("snippet")
                        .getAsJsonObject().get("description")
                        .toString();
                vidDescription[i]=vidDescription[i].substring(1, vidDescription[i].length()-1);

                vidThumbnails[i]=listeVids.get(i)
                        .getAsJsonObject().get("snippet")
                        .getAsJsonObject().get("thumbnails")
                        .getAsJsonObject().get("default")
                        .getAsJsonObject().get("url")
                        .toString();
                vidThumbnails[i]=vidThumbnails[i].substring(1, vidThumbnails[i].length()-1);

                try {
                    URL thumbUrl = new URL(vidThumbnails[i]);
                    bmp[i] = BitmapFactory.decodeStream(thumbUrl.openConnection().getInputStream());
                }
                catch(IOException e) {
                    Log.d("search", e.toString());
                }
            }

            Bundle msgBundle=new Bundle();
            msgBundle.putInt("nbResults", this.nbResults);
            msgBundle.putStringArray("ID", vidId);
            msgBundle.putStringArray("title", vidTitle);
            msgBundle.putStringArray("description", vidDescription);
            msgBundle.putParcelableArray("thumbnails", bmp);
            Message msg=new Message();
            msg.setData(msgBundle);
            handler.sendMessage(msg);
        }
    }
}
