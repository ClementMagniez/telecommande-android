package com.example.telecommande;

import android.content.Context;
import android.net.Network;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageButton;
import androidx.core.content.ContextCompat;

public class ButtonPause extends BooleanButton {

    public ButtonPause(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.configure("play", "pause",
                        R.drawable.ic_media_pause,
                        R.drawable.ic_media_play);
    }
}
