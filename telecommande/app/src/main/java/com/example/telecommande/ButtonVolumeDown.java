package com.example.telecommande;

import android.content.Context;
import android.util.AttributeSet;

public class ButtonVolumeDown extends ButtonAdjustVolume {
    public ButtonVolumeDown(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.configure("down");
    }
}
