package com.example.telecommande;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import androidx.appcompat.widget.AppCompatImageButton;

public class ButtonAdjustVolume extends AppCompatImageButton {

    private String message;

    public ButtonAdjustVolume(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void configure(final String message) {
        this.message=new String(message);
    }

    public void setNetworkThread(final NetworkService networkService) {
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("send", "msg à send au clic : "+ message);
                networkService.setMsg(message);
            }
        });
    }

}
