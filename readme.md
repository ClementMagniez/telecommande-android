Ce repo comporte l'application client-serveur YTRemote, projet universitaire de Clément Magniez et Benjamin Nouvelière.


# Prérequis

- Python 3.x
	- Dépendances non-standard à installer via pip : pynput
- Android Studio (app pas encore déployée)

# Installer

- Cloner le repo ;

# Lancer

## Prérequis

Si l'application n'est pas lancée sur émulateur, changer l'adresse IP : 
- Modifier NetworkService#HOST et NetworkService #PORT dans `telecommande` 
- Modifier HOST et MOBILE_PORT dans `server/server.py`

Avoir une connexion Internet (permet d'effectuer une recherche).
## Exécution

Lancer le serveur :
python3 server.py 

Lancer le client : pour le moment à charger depuis Android Studio.

 
